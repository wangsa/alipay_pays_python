
CREATE TABLE IF NOT EXISTS `alipay_base_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(30) NOT NULL COMMENT '基础配置名',
  `pc_product_code` varchar(30) NOT NULL COMMENT '商家和支付宝签约的销售产品码pc',
  `h5_product_code` varchar(30) NOT NULL COMMENT '商家和支付宝签约的销售产品码h5',
  `appid` varchar(30) NOT NULL COMMENT '开发者应用id',
  `seller_id` varchar(30) NOT NULL COMMENT '支付宝唯一用户号',
  `key` varchar(2000) NOT NULL COMMENT 'app_private_key,开发者应用私钥',
  `exp_time` varchar(10) DEFAULT '0' COMMENT '订单最晚交易时间,取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天',
  `pc_notify_url` varchar(100) NOT NULL COMMENT '服务器异步通知页面路径',
  `h5_notify_url` varchar(100) NOT NULL COMMENT '服务器异步通知页面路径',
  `pc_return_url` varchar(100) NOT NULL COMMENT '页面跳转同步通知页面路径',
  `h5_return_url` varchar(100) NOT NULL COMMENT '页面跳转同步通知页面路径',
  `add_time` int(11) NOT NULL COMMENT '记录添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '修改时间',
  `status` tinyint(2) DEFAULT '1' COMMENT '0删除，1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='支付宝基础配置信息';


CREATE TABLE IF NOT EXISTS `alipay_goods_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alipay_config_id` int(11) NOT NULL COMMENT '支付宝基础配置id',
  `subject` varchar(128) NOT NULL COMMENT '商品名称（最长128个汉字）',
  `body` varchar(255) NOT NULL COMMENT '商品详情',
  `total_amount` varchar(50) NOT NULL COMMENT '交易金额范围[0.01, 100000000.00]精确到小数点后两位',
  `add_time` int(11) NOT NULL COMMENT '记录添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '修改时间',
  `status` tinyint(2) DEFAULT '1' COMMENT '0删除，1正常',
  PRIMARY KEY (`id`),
  KEY `alipay_config_id` (`alipay_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='支付宝商品配置信息';
